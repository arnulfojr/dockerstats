from distutils.core import setup
from setuptools import find_packages

__version__ = '0.1'


__classifiers__ = [
    "Development Status :: 3 - Alpha ",
    "Environment :: Console",
    "Topic :: Utilities",
    "Topic :: Software Development :: Quality Assurance",
    "Topic :: Software Development :: Testing",
    "Topic :: System :: Monitoring",
    "Topic :: System :: Logging",
    "Programming Language :: Python",
    "Programming Language :: Python :: 2.6",
    "Programming Language :: Python :: 2.7",
    "Operating System :: MacOS X",
    "Operating System :: MacOS :: MacOS X",
    "Operating System :: POSIX :: Linux",
    "Intended Audience :: Developers",
    "Intended Audience :: Information Technology",
    "Intended Audience :: System Administrators",
    "License :: Freely Distributable"
]


requires = [
    'docker-py',
    'backports.ssl-match-hostname',
    'funcsigs',
    'ipaddress',
    'requests',
    'six',
    'SQLAlchemy',
    'MySQL-python',
    'py',
    'websocket-client',
    'pbr'
]

test_requires = [
    'pytest',
    'mock'
]


def long_description():
    """
    Loads the README file for the long description
    :return:
    """
    with open("DockerStats/README.md") as readme:
        return readme.read()

setup(
    name='DockerStats',
    description='Docker Statistics Logger',
    long_description=long_description(),
    classifiers=__classifiers__,
    keywords='docker stats statistics',
    url='http://www.myarny.org/DockerStats',
    author='Arnulfo Solis',
    author_email='arnulfojr@kuzzy.com',
    version=__version__,
    license='MIT',
    packages=find_packages(),
    requires=requires,
    test_requires=test_requires,
    test_suite='pytest',
    include_package_data=True
)
