import config
from DockerStats.Persister.Database.exceptions import BaseDatabaseException
from DockerStats.Persister.Database.Configuration.configuration import Connection as DBConnectionConfiguration
from DockerStats.Persister.Database.Configuration.managers import ConnectionManager
from DockerStats.exceptions import *
from DockerStats.Listener import get

if __name__ == '__main__':  # Execute the listen/loop

    try:
        db_conf = DBConnectionConfiguration(
            user=config.DB_USER,
            password=config.DB_PASS,
            schema=config.DB_SCHEMA,
            host=config.DB_HOST,
            port=config.DB_PORT
        )
        cm = ConnectionManager(config=db_conf, auto_connect=True)

        strategy = get(config.PARALLEL)  # get the Strategy
        strategy.listen(manager=cm, config=config)  # listen and execute the protocol

    except BaseDatabaseException, e:
        print e.get_sql_message()
    except DockerApiBaseException, e:
        print e.message
