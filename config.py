import os

# --- new API --- #
PARALLEL = os.getenv("PARALLEL", 'ENABLED') == 'ENABLED'

# --- Database configuration --- #
DB_HOST = os.getenv("DB_HOST", "0.0.0.0")
DB_PORT = os.getenv("DB_PORT", 3306)
DB_USER = os.getenv("DB_USER", "")
DB_PASS = os.getenv("DB_PASS", "")
DB_SCHEMA = os.getenv("DB_SCHEMA", "docker_stats")

# --- Docker socket --- #
DOCKER_URL = os.getenv("DOCKER_URL", "unix:///var/run/docker.sock")

# --- Container to get stats from --- #
CONTAINER_ID = os.getenv("CONTAINER_ID", "26131a1783a8")

# --- Interval --- #
INTERVAL = os.getenv("DEFAULT_INTERVAL", 5)  # in seconds
SAMPLES = os.getenv("DEFAULT_SAMPLES", 60 * 3)  # Default number of samples

# --- DB related stuff --- #
REPOSITORY = "DockerStats.Persister.Database.Repositories.StateRepository.StateRepository"
BUILDER = dict()
BUILDER["simple"] = "DockerStats.Persister.Builders.StateBuilder.StateBuilder"

# --- Extractors to get stats from --- #
EXTRACTORS = dict()
EXTRACTORS["simple"] = {
    "extractor": None,  # use default
    "processor": "DockerStats.Stats.Processors.SimpleDockerStats.SimpleStatsProcessor",
    "caller": None  # use default
}
EXTRACTORS["cpu_percentage"] = {
    "extractor": None,  # use default
    "processor": "DockerStats.Stats.Processors.SimpleCPUDockerStats.SimpleCPUProcessor",
    "caller": None  # use default
}

# --- Register your Extractors --- #
# ...
# --- End Dynamic extractors --- #


# --- Containers to be ran parallel --- #
CONTAINERS = dict()
CONTAINERS["3144a79da496"] = {
    "Extractor": "cpu_percentage",
    "Interval": 1,
    "Samples": 60,
    "SampleInterface": "DockerStats.Samples.BaseSample.BaseSample",
    "LoggerInterface": "DockerStats.Sample.BaseSample.BaseLogger",
    "LoggerProperties": {
        "Repository": "DockerStats.Persister.Database.Repositories.StateRepository.StateRepository",
        "Entity": "DockerStats.Persister.Database.Entities.ResourceCPUState",
        "Builder": "DockerStats.Persister.Builders.StateBuilder.StateCPUBuilder"
    }
}
