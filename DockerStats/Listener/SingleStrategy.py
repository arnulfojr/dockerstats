import time
from StrategyInterface import ListenerStrategy
from DockerStats.Persister.Database.Repositories.factory import RepositoryFactory
from DockerStats.Stats.Factory import StatsFactory
from DockerStats.Persister.Builders import BuilderFactory


class SingleStrategy(ListenerStrategy):
    """
    Strategy to sample only one container in the current main thread
    """
    def __init__(self):
        ListenerStrategy.__init__(self)

    def listen(self, manager, config, *args, **kwargs):
        """
        Listens the Docker API in a synchronous way
        :param manager:
        :param config:
        :param args:
        :param kwargs:
        :return:
        """
        repo = RepositoryFactory.create(full_repository_class=config.REPOSITORY, manager=manager)
        extractor = StatsFactory.get(
            container_info=config.EXTRACTORS["simple"],
            caller_args={'base_url': config.DOCKER_URL}
        )
        for i in range(0, config.SAMPLES):
            try:
                results = extractor.extract(container=config.CONTAINER_ID)
                print ("sample (%s/%s):" % (i, config.SAMPLES)), results
                builder = BuilderFactory.create(full_builder_class=config.BUILDER["simple"])
                state = builder.build(data=results)
                repo.insert(entity=state, auto_commit=True)
                time.sleep(config.INTERVAL)
            except Exception as e:
                print e.message
        return
