import ParallelStrategy
import SingleStrategy


def get(new_api=True):
    """
    Returns the strategy depending on the parameter
    :param new_api:
    :return:
    """
    if new_api:
        return ParallelStrategy.ParallelStrategy()
    return SingleStrategy.SingleStrategy()
