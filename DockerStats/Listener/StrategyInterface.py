class ListenerStrategy(object):
    """
    Listener strategy interface
    """
    def listen(self, *args, **kwargs):
        """
        :param args:
        :param kwargs:
        :return:
        """
        raise NotImplementedError
