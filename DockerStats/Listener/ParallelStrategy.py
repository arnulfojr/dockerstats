from StrategyInterface import ListenerStrategy
from DockerStats.Samples.Loader import SampleLoader
from DockerStats.Utility.Executor.Process import BulkManager
from DockerStats.Utility.Executor.Protocol import execute_protocol


class ParallelStrategy(ListenerStrategy):
    """
    Parallel strategy
    """
    def __init__(self):
        ListenerStrategy.__init__(self)

    def listen(self, manager, config, *args, **kwargs):
        """
        Listens the Docker API in a parallel way
        :param manager:
        :param config:
        :param args:
        :param kwargs:
        :return:
        """
        containers_to_sample = config.CONTAINERS
        executor = BulkManager(task=execute_protocol)
        # contains all the containers to sample in their own wrapper
        wrappers = SampleLoader.load(
            containers=containers_to_sample,
            manager=manager,
            socket=config.DOCKER_URL,
            extractors=config.EXTRACTORS
        )  # Use the default sample class and logger class, no extra kwargs

        executor.bulk_process(
            wrappers.values(),
            clear=True,
            parameter_name="wrapper"
        )
