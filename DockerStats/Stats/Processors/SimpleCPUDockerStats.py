from SimpleDockerStats import SimpleStatsProcessor


class SimpleCPUProcessor(SimpleStatsProcessor):
    """
    Simple CPU processor
    """
    def __init__(self):
        SimpleStatsProcessor.__init__(self)
        self.previous_reading = 0
        self.previous_system_reading = 0

    @staticmethod
    def calculate_cpu_percentage(previous_cpu, previous_system_cpu, cpu_stats):
        """
        Adapted from the docker API
        https://github.com/docker/docker/blob/master/api/client/container/stats_helpers.go#L203-L216
        :param previous_cpu:
        :param previous_system_cpu:
        :param cpu_stats:
        :return:
        """
        cpu_percentage = 0
        cpu_delta = cpu_stats["cpu"]["total_usage"] - previous_cpu
        system_delta = cpu_stats["cpu"]["system_usage"] - previous_system_cpu
        if system_delta > 0 and cpu_delta > 0:
            cpu_percentage = (float(cpu_delta) / float(system_delta)) * 100 * cpu_stats["cpu"]["count"]
        return cpu_percentage

    def process(self, stats, **kwargs):
        """
        Reduces the Stats to a more simple format
        :param stats:
        :param kwargs:
        :return:
        """
        extracted_stats = super(SimpleCPUProcessor, self).process(stats, **kwargs)  # get the stats from the parent
        extracted_stats["cpu"]["system_usage"] = stats["cpu_stats"]["system_cpu_usage"]
        extracted_stats["cpu"]["count"] = len(stats["cpu_stats"]["cpu_usage"]["percpu_usage"])
        cpu_percentage = SimpleCPUProcessor.calculate_cpu_percentage(
            previous_cpu=self.previous_reading,
            previous_system_cpu=self.previous_system_reading,
            cpu_stats=extracted_stats
        )
        extracted_stats["cpu"]["percentage"] = cpu_percentage
        self.previous_reading = extracted_stats["cpu"]["total_usage"]
        self.previous_system_reading = extracted_stats["cpu"]["system_usage"]
        return extracted_stats
