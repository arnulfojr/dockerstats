class Processor(object):
    """
    Processor interface
    """
    def process(self, *args, **kwargs):
        raise NotImplementedError
