from Processor import Processor


class SimpleStatsProcessor(Processor):
    """
    Extracts the CPU %, Memory
    """
    def __init__(self):
        Processor.__init__(self)

    @staticmethod
    def process_stat(stat):
        """
        Items to get from the stats
        https://docs.docker.com/engine/reference/api/docker_remote_api_v1.24/
        :param stat:
        :return:
        """
        simple_stats = dict()
        simple_stats["memory"] = dict()
        simple_stats["cpu"] = dict()
        # --- Memory stats --- #
        simple_stats["memory"]["limit"] = stat["memory_stats"]["limit"]
        simple_stats["memory"]["usage"] = stat["memory_stats"]["usage"]
        simple_stats["memory"]["max_usage"] = stat["memory_stats"]["max_usage"]
        # --- CPU usage --- #
        simple_stats["cpu"]["total_usage"] = stat["cpu_stats"]["cpu_usage"]["total_usage"]
        return simple_stats

    def process(self, stats, **kwargs):
        """
        Reduces the Stats to a more simple format
        :param stats:
        :param kwargs:
        :return:
        """
        simple_stats = SimpleStatsProcessor.process_stat(stats)
        return simple_stats
