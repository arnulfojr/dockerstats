class Caller(object):
    """
    Caller interface
    """
    def call(self, **kwargs):
        raise NotImplementedError
