from Caller import Caller
from docker import Client


class DockerStats(Caller):
    """
    Docker stats caller
    https://docker-py.readthedocs.io/en/stable/api/#client-api
    """
    def __init__(self, base_url):
        Caller.__init__(self)
        self.base_url = base_url
        self.docker_client = Client(base_url=base_url)

    def consult(self, container):
        """
        Consults the stats of the Docker container
        :param container:
        :return:
        """
        return self.docker_client.stats(container, stream=False)

    def call_iterative(self, containers):
        """
        Calls the stats of the list of containers
        :param containers:
        :return:
        """
        stats = dict()
        for container in containers:
            stats[container] = self.consult(container)
        return stats

    def call(self, **kwargs):
        """
        Call the docker stats
        :param kwargs:
        :return:
        """
        containers = kwargs.get("containers", None)
        container = kwargs.get("container", None)
        if container is None and containers is None:
            raise RuntimeError
        if containers:
            return self.call_iterative(containers)
        if container:
            stats = self.consult(container)
            return stats
        return None
