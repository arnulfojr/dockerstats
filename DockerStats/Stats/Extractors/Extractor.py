class Extractor(object):
    """
    Extractor interface
    """
    def extract(self, *args, **kwargs):
        raise NotImplementedError
