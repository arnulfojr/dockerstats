from Extractor import Extractor


class DockerStats(Extractor):
    """
    Extracts the simple stats of the container
    """
    def __init__(self, caller, processor):
        self.caller = caller
        self.processor = processor

    def extract(self, container, **kwargs):
        """
        Gets the container(s) and gets the stats
        :param container:
        :raises IndexError:
        :return:
        """
        stats = self.caller.call(container=container)
        stats = self.processor.process(stats=stats)
        return stats
