from DockerStats.Utility.loaders import ClassLoader


class StatsFactory(object):
    """
    Factory to load all the extractors processors and callers needed to execute the stats calling
    """

    DEFAULT_CALLER = "DockerStats.Stats.Callers.Docker.DockerStats"
    DEFAULT_EXTRACTOR = "DockerStats.Stats.Extractors.Docker.DockerStats"
    DEFAULT_PROCESSOR = "DockerStats.Stats.Processors.SimpleDockerStats.SimpleStatsProcessor"

    @staticmethod
    def get(container_info, caller_args={}, processor_args={}):
        """
        Returns the extractor instantiated
        :param container_info:
        :param caller_args:
        :param processor_args:
        :return: Extractor
        """
        if "caller" not in container_info or container_info["caller"] is None:
            container_info["caller"] = StatsFactory.DEFAULT_CALLER
        if "extractor" not in container_info or container_info["extractor"] is None:
            container_info["extractor"] = StatsFactory.DEFAULT_EXTRACTOR
        if "processor" not in container_info or container_info["processor"] is None:
            container_info["processor"] = StatsFactory.DEFAULT_PROCESSOR
        # --- Load the classes --- #
        extractor = ClassLoader.load(container_info.get("extractor", StatsFactory.DEFAULT_EXTRACTOR))
        processor = ClassLoader.load(container_info.get("processor", StatsFactory.DEFAULT_PROCESSOR))
        caller = ClassLoader.load(container_info["caller"])
        # --- instantiate the extractor --- #
        caller_instance = caller(**caller_args)
        processor_instance = processor(**processor_args)
        extractor = extractor(caller=caller_instance, processor=processor_instance)
        return extractor
