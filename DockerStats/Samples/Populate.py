class Populate(object):
    """
    Interface for Builders to implement.
    """
    def populate(self, instance, **kwargs):
        """
        Meant to populate the instance passed with the given data
        @param instance: Instance
        @param kwargs: dict
        @return: Instance populated
        """
        raise NotImplementedError


class SampleWrapperPopulate(Populate):
    """
    Sample Wrapper Builder
    """
    def __init__(self):
        Populate.__init__(self)

    @staticmethod
    def populate(instance, **kwargs):
        """
        Populates the instance with the given arguments,
        meant to fill a Sample instance
        :param instance:
        :param kwargs:
        :return:
        """
        instance.socket = kwargs.get("Socket", "")
        instance.container_id = kwargs.get("Container_id", "")
        instance.extractor = kwargs.get("Extractor", None)
        instance.interval = kwargs.get("Interval", 1)
        instance.samples = kwargs.get("Samples", 5)
        return instance


class LoggerWrapperPopulate(Populate):
    """
    Populates the Logger Wrapper
    """
    def __init__(self):
        Populate.__init__(self)

    @staticmethod
    def populate(instance, **kwargs):
        """
        Populates an instance of Logger
        :param instance:
        :param kwargs:
        :return:
        """
        instance.repository = kwargs.get("Repository", None)
        instance.builder = kwargs.get("Builder", None)
        return instance
