class Sample(object):
    """
    Sample interface
    """
    def __init__(self):
        self._socket = None
        self._container_id = None
        self._interval = 0
        self._extractor = None
        self._samples = 0
        self._logger = None

    @property
    def socket(self, *args, **kwargs):
        raise NotImplementedError

    @property
    def container_id(self, *args, **kwargs):
        raise NotImplementedError

    @property
    def extractor(self, *args, **kwargs):
        raise NotImplementedError

    @property
    def interval(self, *args, **kwargs):
        raise NotImplementedError

    @property
    def samples(self, *args, **kwargs):
        raise NotImplementedError

    @property
    def logger(self, *args, **kwargs):
        raise NotImplementedError


class Logger(object):
    """
    This is the logger interface
    """
    def __init__(self):
        self._repository = None
        self._builder = None

    @property
    def repository(self, *args, **kwargs):
        raise NotImplementedError

    @property
    def builder(self, *args, **kwargs):
        raise NotImplementedError
