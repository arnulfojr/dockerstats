from ..Utility.loaders import ClassLoader
from ..Stats.Factory import StatsFactory
from ..Persister.Database.Repositories.factory import RepositoryFactory
from .Populate import LoggerWrapperPopulate, SampleWrapperPopulate


class SampleLoader(object):
    """
    This class is meant to load from the configuration a valid
    structure
    """

    DEFAULT_SAMPLE = "DockerStats.Samples.BaseSample.BaseSample"
    DEFAULT_LOGGER = "DockerStats.Samples.BaseSample.BaseLogger"
    DEFAULT_EXTRACTOR = "simple"

    @staticmethod
    def load(containers, manager, socket, extractors, **kwargs):
        """
        Loads a valid list of Samples from a raw dictionary.
        :param containers:
        :param manager:
        :param socket:
        :param extractors:
        :return:
        """
        samples = {}

        for key in containers:
            data = containers[key]
            if "SampleClass" not in data or data["SampleClass"] is None:  # Use the default sample
                data["SampleClass"] = SampleLoader.DEFAULT_SAMPLE
            if "LoggerClass" not in data or data["SampleClass"] is None:  # Use the default logger
                data["LoggerClass"] = SampleLoader.DEFAULT_LOGGER
            sample_class = ClassLoader.load(data["SampleClass"])
            logger_class = ClassLoader.load(data["LoggerClass"])
            sample_wrapper = sample_class()

            extractor = extractors[data.get("Extractor", SampleLoader.DEFAULT_EXTRACTOR)]
            data["Extractor"] = StatsFactory.get(
                container_info=extractor,
                caller_args={'base_url': socket}
            )
            sample_wrapper = SampleWrapperPopulate.populate(
                instance=sample_wrapper,
                Container_id=key,
                Socket=socket,
                **data)
            logger_wrapper = LoadLogger.load(logger_class, manager, **(data.get("LoggerProperties", None)))
            sample_wrapper.logger = logger_wrapper
            samples[key] = sample_wrapper

        return samples


class LoadLogger(object):
    """
    Loads the logger
    """

    DEFAULT_REPOSITORY = "DockerStats.Persister.Repositories.StateRepository.StateRepository"
    DEFAULT_ENTITY = "DockerStats.Persister.Database.Entities.ResourceState"
    DEFAULT_BUILDER = "DockerStats.Persister.Builders.StateBuilder.StateBuilder"

    @staticmethod
    def load_repository(logger_wrapper, manager):
        """
        Loads the repository in the logger wrapper
        :param logger_wrapper:
        :param manager:
        :return:
        """
        repo = RepositoryFactory.create(
            full_repository_class=logger_wrapper.repository,
            manager=manager
        )
        logger_wrapper.repository = repo
        return logger_wrapper

    @staticmethod
    def load_builder(logger_wrapper):
        """
        Loads the builder in the logger wrapper
        :param logger_wrapper:
        :return:
        """
        builder_class = ClassLoader.load(logger_wrapper.builder)
        logger_wrapper.builder = builder_class()
        return logger_wrapper

    @staticmethod
    def load(logger_class, manager, **kwargs):
        """
        Loads the logger
        :param logger_class:
        :param manager:
        :param kwargs:
        :return:
        """
        logger = logger_class()
        logger = LoggerWrapperPopulate.populate(logger, **kwargs)
        logger = LoadLogger.load_repository(logger, manager)
        logger = LoadLogger.load_builder(logger)
        return logger
