from .Sample import Sample, Logger


class BaseLogger(Logger):
    """
    Base logger for persisting the docker statistics
    """
    def __init__(self):
        Logger.__init__(self)
        self._repository = None
        self._builder = None
        self.entity = None

    @property
    def repository(self):
        return self._repository

    @repository.setter
    def repository(self, repository):
        self._repository = repository

    @property
    def builder(self):
        return self._builder

    @builder.setter
    def builder(self, builder):
        self._builder = builder


class BaseSample(Sample):
    """
    Base Sample for logging the default statistics
    """
    def __init__(self):
        Sample.__init__(self)

    @property
    def socket(self):
        return self._socket

    @socket.setter
    def socket(self, socket_str):
        self._socket = socket_str

    @property
    def container_id(self):
        return self._container_id

    @container_id.setter
    def container_id(self, container_id):
        self._container_id = container_id

    @property
    def extractor(self):
        return self._extractor

    @extractor.setter
    def extractor(self, extractor):
        self._extractor = extractor

    @property
    def interval(self):
        return self._interval

    @interval.setter
    def interval(self, interval):
        self._interval = int(interval)

    @property
    def samples(self):
        return self._samples

    @samples.setter
    def samples(self, samples):
        self._samples = samples

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, logger):
        self._logger = logger
