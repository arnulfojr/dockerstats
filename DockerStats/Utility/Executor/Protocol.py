from datetime import datetime
import time
from ...Persister.Database.Repositories.factory import RepositoryFactory


def execute_protocol(wrapper):
    """
    Samples the container
    :param wrapper:
    :return:
    """
    try:
        print "[{timestamp}] Will sample {container_id}".format(
            timestamp=datetime.now().strftime("%X, %d %b"),
            container_id=wrapper.container_id
        )
        session_repo = RepositoryFactory.create(
            full_repository_class="DockerStats.Persister.Database.Repositories.SessionRepository.SessionRepository",
            manager=wrapper.logger.repository.manager
        )
        session = session_repo.create_session(container_id=wrapper.container_id)
        create_metadata = True

        for i in range(0, wrapper.samples):
            container_id = wrapper.container_id
            builder = wrapper.logger.builder
            results = wrapper.extractor.extract(container=container_id)
            state = builder.build(data=results)

            print "[{timestamp}] - Will log state ({i}/{samples}) from {container_id}".format(
                timestamp=datetime.now().strftime("%X, %d %b"),
                i=i+1,
                samples=wrapper.samples,
                container_id=container_id
            )

            wrapper.logger.repository.insert(
                entity=state,
                log_session=session,
                auto_commit=True,
                create_metadata=create_metadata
            )

            create_metadata = False
            time.sleep(wrapper.interval)
    except Exception, e:
        print "Sampling got error and exited, {error_message}".format(error_message=e.message)
