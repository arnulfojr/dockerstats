from ..KeyGenerator import KeyGenerator
from .DSProcess import DSProcess


class ProcessExecutor(object):
    """
    Executes a process with the information given.
    """

    def __init__(self):
        self.__processes = {}
        self.__bulks = {}  # Bulks work in a separate thread executing multiple threads, one thread controlling threads

    def get_processes_keys(self):
        results = []
        for key in self.__processes:
            results.append(key)
        return results

    def stop_all_processes(self, auto_clear=False):
        """
        Stops all processes.
        :param auto_clear: Boolean, clears all processes ID
        :return:
        """
        DSProcess.exit_flag = 1

        for key in self.__processes:
            self.__processes[key].join()

        for key in self.__processes:
            if self.__processes[key].is_alive():
                return False
        if auto_clear:
            return self.clear_processes()
        return True

    def join_process(self, process_reference):
        """
        Wait for process to end
        :param process_reference: string
        :return:
        """
        if process_reference in self.__processes:
            self.__processes[process_reference].join()

    def clear_processes(self):
        """
        Clears the current holding processes
        :return: Boolean
        """
        if self.get_live_processes_count() > 0:
            return False
        self.__processes = {}
        return True

    def is_bulk_alive(self, reference):
        """
        Checks whether the passed BulkManager process reference is alive.
        :param reference: string, Bulk reference
        :return: Boolean
        """
        if reference in self.__bulks:
            return self.__bulks[reference]["process"].is_alive()
        return False

    def get_live_processes_count(self):
        """
        Returns the current alive processes it has.
        :return: integer
        """
        count = 0
        for key in self.__processes:
            if self.__processes[key].is_alive():
                count += 1
        return count

    def stop_process(self, process_id):
        """
        Attempts to close a holded process and waits for it to end
        :param process_id: string
        :return: True when process joined the thread
        """
        process = self.__processes[process_id]
        if process.is_alive():
            process.stop()
            process.join()
        return True

    def execute_bulk(self, task, **params):
        """
        Executes a Bulk
        :param task: Pointer to a method
        :param params: dict, parameters of the function
        :return: string, key of the BulkManager process
        """
        key = KeyGenerator.get_unique_key()
        bulk_manager = BulkManager(task)
        new_process = DSProcess(process_id=key, process=bulk_manager.bulk_process, loop=False, **params)
        new_process.start()
        self.__bulks[key] = {"process": new_process, "manager": bulk_manager}
        return key

    def get_a_process(self):
        """
        Sets an allocated thread without executing it.
        :return: processId (string), WCProcess instance
        """
        for key in self.__processes:
            if not self.__processes[key].is_alive():
                process = DSProcess(process_id=key)
                self.__processes[key] = process
                return key, process
        key = KeyGenerator.get_unique_key()
        self.__processes[key] = DSProcess(process_id=key)
        return key, self.__processes[key]

    def execute(self, task, auto_start=True, loop=False, **process_parameters):
        """
        Executes an individual method with the passed parameters
        :param task: Pointer to a method
        :param auto_start: Boolean
        :param loop: Boolean
        :param process_parameters: dict
        :return: Process Id (string)
        """
        if DSProcess.exit_flag == 1:
            DSProcess.exit_flag = 0
        key, process = self.get_a_process()
        process.set_process(task, loop, **process_parameters)
        if auto_start:
            process.start()
        return key


class BulkManager(object):
    """
    Manages the Bulk/Batch processes, this thread lives until all sub-processes end.
    """
    def __init__(self, task, process_executor=None):
        self.__executor = process_executor
        if not self.__executor:
            self.__executor = ProcessExecutor()
        self.__stop = 0
        self.__task = task
        self.__processes = []

    def __join_processes(self):
        for process in self.__processes:
            self.__executor.stop_process(process)

    def clear(self):
        """
        Clears all processes ID it holds
        :return:
        """
        self.__executor.clear_processes()
        self.__processes = []

    def get_processes(self):
        return self.__processes

    def bulk_process(self, bulk, clear=False, **params):
        """
        Executes the process by the size specified.
        :param bulk: Number of process to allocate
        :param clear: Boolean, clears the previous processes Id's
        :param params: the function's parameters
        :return:
        """
        if clear:
            self.clear()

        parameter_name = params.pop("parameter_name")
        for x in bulk:  # create threads
            new_params = {parameter_name: x}
            new_params.update(params)
            self.__processes.append(
                self.__executor.execute(
                    self.__task, auto_start=True, loop=False, **new_params
                )
            )
        self.__join_processes()
        while self.__executor.get_live_processes_count() > 0:
            print "waiting for processes to end"
