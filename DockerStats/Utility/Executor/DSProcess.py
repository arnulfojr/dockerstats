import threading


class DSProcess(threading.Thread):
    """
    Thread wrapper to use in DockerStats
    """
    exit_flag = 0

    def __init__(self, process_id, process=None, loop=False, **process_parameters):
        threading.Thread.__init__(self)
        self.__process_id = process_id
        self.__process = process
        self.__exit_flag = 0
        self.__loop = loop
        self.__process_args = process_parameters

    def set_process(self, new_process, loop=False, **kwargs):
        """
        Sets the process to execute and its proper configuration elements.
        :param new_process:
        :param loop:
        :param kwargs:
        :return:
        """
        self.__process = new_process
        self.__process_args = kwargs
        self.__loop = loop

    def stop(self):
        """
        Stops all Looping Processes.
        :return:
        """
        self.__exit_flag = 1

    def run(self):
        """
        Runs the Process with the current configuration state.
        :return: False, if exception occured
        """
        try:
            if self.__loop:
                while DSProcess.exit_flag == 0 and self.__exit_flag == 0:
                    self.__process(**self.__process_args)
            else:
                self.__process(**self.__process_args)
        except Exception, e:
            e.message = "Threaded exception: %s" % e.message
            return False
