from uuid import uuid4


class KeyGenerator(object):
    """
    Pseudo-random-unique Key generator
    """
    @staticmethod
    def get_unique_key():
        """
        Generates a pseudo-random and unique key string from UUID4 standards
        :return:
        """
        return uuid4()

    @staticmethod
    def get_unique_id(repo):
        """
        It finds a unique id not assigned in the repository
        :param repo:
        :return:
        """
        key = KeyGenerator.get_unique_key()
        if repo.is_unique_id_assigned(key):
            return KeyGenerator.get_unique_id(repo)
        return key
