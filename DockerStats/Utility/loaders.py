from ..exceptions import LoadingError


class ClassLoader(object):
    """
    This class loader loads all dynamic classes
    """
    @staticmethod
    def load(full_class_name):
        """
        Loads the class in the given string.
        The string contains the full_package.class_name.
        NOTE: Loaded class is not the same as instantiated class.
        @param full_class_name: string
        @return: Loaded class
        """
        import importlib
        try:
            module_name, class_name = full_class_name.rsplit(".", 1)  # divide the module name and the class name
            new_class = getattr(importlib.import_module(module_name), class_name)  # import and get the Class
            return new_class
        except Exception as e:
            raise LoadingError(message=e.message, previous=e)
