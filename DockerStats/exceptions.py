class DockerApiBaseException(Exception):
    """
    Base exception
    """
    def __init__(self, message, code=500, previous=None):
        Exception.__init__(self)
        self.message = message
        self.code = code
        self.previous = previous


class LoadingError(DockerApiBaseException):
    """
    Loading error
    """
    def __init__(self, message=None, code=500, previous=None):
        DockerApiBaseException.__init__(self, message=message, code=code, previous=previous)
