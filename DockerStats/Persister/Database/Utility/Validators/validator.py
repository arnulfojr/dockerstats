class Validator(object):
    """
    Validator interface
    """
    @staticmethod
    def validate(target, **kwargs):
        """
        Validates the target
        :param target:
        :param kwargs:
        :return:
        """
        raise NotImplementedError
