from validator import Validator


class ManagerInstanceValidator(Validator):
    """
    Validates the target to be an instance of
    """
    def __init__(self):
        Validator.__init__(self)

    @staticmethod
    def validate(target, **kwargs):
        """
        validates the target is an instance of DefaultEntityManager
        :param target:
        :param kwargs:
        :return:
        """
        from ...Configuration.managers import Manager
        return isinstance(target, Manager)


class RepositoryInstanceValidator(Validator):
    """
    Validates the target to be a repository
    """
    def __init__(self):
        Validator.__init__(self)

    @staticmethod
    def validate(target, **kwargs):
        """
        Validates the target as being a Repository instance
        :param target:
        :param kwargs:
        :return:
        """
        from ...Repositories.repositories import Repository
        return isinstance(target, Repository)


class RepositoryClassValidator(Validator):
    """
    Validates the Class to be a Repository one
    """
    def __init__(self):
        Validator.__init__(self)

    @staticmethod
    def validate(target, **kwargs):
        """
        Validates that the target is subclass of the Repository instance
        :param target:
        :param kwargs:
        :return:
        """
        from ...Repositories.repositories import Repository
        return issubclass(target, Repository)
