Database
========

Introduction
------------
+ Main goal of this library is to offer a concrete way of implementing
the ORM ```SQLAlchemy``` into a scrapy spider.
+ Holds the implementation of a ORM library into Kayak Spider.
    - ORM to implement: ```SQLAlchemy 1.0```
+ This implementation offers a Docker-Compose file in order to test
the implementation in a local Database.
    - ```docker-compose up``` in the current directory, this holds the
    same credentials registered in the Kayak Spider.
        + Exposes the database in the port ```5002``` by default.
        + Beware! The database maps a local volume.
+ All exceptions raised by the database package are wrapped around the
exception ```BaseDatabaseException```, for further information refer
to ```database/exceptions.py```.

Entities
--------
+ For all objects mapped in the database, we will refer as ```Entities```,
as this objects are the object targeted to map in the database.
+ Declare all Entities in the ```entities.py``` file.
    - They all use a default collation which is defined in the same file.
    
Relevance of the Entity Manager
-------------------------------
+ The package offers a session wrapper called Entity Manager, as manages
the Entities in the current transaction/session.
+ This wrapper is important as this class cares also for the creation
and error handling of the SQLAlchemy engine and session.
+ The most important use of the the ```EntityManager``` is that, it holds
only one scoped session. This gives use the advantage to handle one
scoped session, therefore one ```EntityManager```, per Pipeline.

Relevance of Repositories
-------------------------
+ The relevance of the repositories lay in the complete division of the
Database Queries, independently of the type of query, from the application
logic.
+ At the same time, it allows isolation of each database queries depending
of the type of entity.
+ Also allows isolation of the query and it's proper error handling.

> A goal is to have one repository per Entity, although is not necessary
and does not fulfil every case scenario, it will allows a better query
organization within the application.

Relevance of Builders
---------------------
+ Builders allow more flexibility on the fly and in maintenance, by
defining them in the Spider setting's we can extract them and load them
with the ```BuilderFactory```, this allows use to edit only the builder
given to the entity and quickly change the mapping between the entity and
the source element.
+ Strategic advantage of defining a Builder for every item type/source to
be persisted:
    - When the source item changes, if so, then the Entity wont change,
    what will change will be the mapping, hence the Builder.
    - If the builder itself is deprecated and/or no longer used, then
    a better approach is to redefine or rewrite the builder, hence redo the
    mapping. What this library allows is to quickly change the Builder
    with only changing the Builder class in the Settings of the Spider.
        + ```hydra.settings``` has an option defined as ```BUILDERS```,
        this maps the Builder to use for each Entity stored in the
        database.

> For further information refer to the builder's README file.

How to use
----------
+ For better understanding on how Database works consult the following code:
```python
# ...
import config
from Database.configuration.configuration import Connection as ConnectionConfiguration
from Database.configuration.entity_manager import DefaultEntityManager as EntityManager
from Database.repositories.factory import RepositoryFactory
# ...
connection_config = ConnectionConfiguration(
    user=config.DB_USER,
    password=config.DB_PASS,
    schema="docker_states",
    host=config.DB_HOST,
    port=config.DB_PORT
)
entity_manager = EntityManager(config=connection_config, auto_connect=True)
repository = RepositoryFactory.create(
    full_repository_class=config.MY_REPO,
    entity_manager=entity_manager
)
# or in init
repository = SomeRepository(entity_manager=entity_manager)
# ...
repo.add_some_entity(entity=mock_entity)  # for example
```

Tests
-----
+ The database package comes with the respective tests.
+ Used Py.test to execute such tests.
    - If you do not want to execute the entity manager test, which calls
    the creation of the schema, exclude the mark ```entity_manager``` from
    ```Py.test``` execution.
    > pytest -m 'not entity_manager'
    
Contact
-------
+ Arnulfo Solis
    - arnulfo.solis@trivago.com