class Connection:
    """
    Connection URL Builder.
    """
    def __init__(self, user, password="", driver=None, schema="", host="localhost", port=None,
                 dialect="mysql", charset=None):
        """
        Specify the configuration of the connection
        :param user:
        :param password:
        :param driver:
        :param schema:
        :param host:
        :param port:
        :param dialect:
        :param charset:
        """
        self.__user = user
        self.__password = password
        self.__driver = driver
        self.__schema = schema
        self.__host = host
        self.__port = port
        self.__dialect = dialect
        self.__charset = charset

    @property
    def charset(self):
        if not self.__charset:
            return "utf-8"
        return self.__charset

    @charset.setter
    def charset(self, charset):
        self.__charset = charset

    def set_password(self, p):
        self.__password = p
        return self.__password

    def set_driver(self, d):
        self.__driver = d
        return self.__driver

    def set_schema(self, s):
        self.__schema = s
        return self.__schema

    def set_host(self, h):
        self.__host = h
        return self.__host

    def set_port(self, p):
        self.__port = p
        return self.__port

    def set_dialect(self, d):
        self.__dialect = d
        return self.__dialect

    def get_dialect(self):
        """
        Returns the Dialect in the URL format.
        :return: String part of the Dialect URL
        """
        dialect = "%s" % self.__dialect
        if self.__driver:
            dialect = "%s+%s" % (dialect, self.__driver)
        return dialect

    def get_authentication_parameters(self):
        """
        Returns the Authentication in a URL format (only partial)
        :return: String part of the Authentication URL
        """
        if self.__password:
            authentication = "%s:%s" % (self.__user, self.__password)
        else:
            authentication = ""
        return authentication

    def get_database_target(self):
        """
        Returns the Targeted Database in a URL format (only partial)
        :return: String part of the targeted database
        """
        target = self.__host
        if self.__port:
            target = "%s:%s" % (target, self.__port)
        target = "%s/%s" % (target, self.__schema)
        return target  # TODO: add the charset option to the URL

    def get_url(self):
        """
        Builds the final URL connection, on the current status of the instance.
        :return: URL as string
        """
        return "%s://%s@%s" % (
            self.get_dialect(),
            self.get_authentication_parameters(),
            self.get_database_target()
        )
