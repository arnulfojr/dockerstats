Configuration - Database
========================

Relevance of the Entity Manager
-------------------------------
+ The package offers a session wrapper called Entity Manager, as manages
the Entities in the current transaction/session.
+ This wrapper is important as this class cares also for the creation
and error handling of the SQLAlchemy engine and session.
+ The most important use of the the ```EntityManager``` is that, it holds
only one scoped session. This gives use the advantage to handle one
scoped session, therefore one ```EntityManager```, per Pipeline.
+ The dynamic allocation of repositories give the advantage of allocating
the Repositories in the most flexible way, by giving the desired EntityManager
 to each repository.
    - This is done in the Pipeline.

Contact
-------
+ Arnulfo Solis
    - arnulfo.solis@trivago.com