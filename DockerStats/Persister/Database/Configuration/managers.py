from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import scoped_session, sessionmaker
from ...ManagerInterface import Manager
from ..Entities.BaseEntities import Base
from ..exceptions import DatabaseException


class ConnectionManager(Manager):
    """
    Defines the connection to the database and session manager
    This class is meant to be extended from if your application
    wishes to use the Repository loader!
    """
    def __init__(self, config, auto_connect=True):
        """
        By setting the auto_connect attribute to True, DefaultEntityManager will automatically
        connect to the database and check if the schema was created to fit the requirements needed
        stated by the Entities.
        @param config: Configuration.Connection
        @param auto_connect: Boolean
        """
        Manager.__init__(self)
        self.__connection_config = config
        self.__engine = None
        self.__session = None
        if auto_connect:
            self.set_up()

    def set_up(self):
        """
        Attempts to set up the connection to the database with the given configuration.
        @raise OperationalError:
        @return True: If succeeded, else raises exception
        """
        try:
            self.__engine = create_engine(self.__connection_config.get_url())
            self.__session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=self.__engine))
            Base.metadata.create_all(self.__engine, checkfirst=True)
            return True
        except OperationalError as e:
            raise DatabaseException(message=e.message, previous=e)

    def connect(self):
        """
        Expose the set up method through a better naming
        @return True: If succeeded, else raises exception
        """
        return self.set_up()

    def disconnect(self):
        """
        Exposes a disconnect method, in order to clean up!
        @return:
        """
        self.__session.remove()
        return True

    def set_up_with(self, engine, session):
        """
        Use this method if you wish to switch the session and engine, applies only for SQLAlchemy instances!
        @param engine: Engine instance
        @param session: Session instance
        @return:
        """
        self.set_session(session)
        self.set_engine(engine)
        Base.metadata.create_all(self.__engine, checkfirst=True)

    def get_connection(self):
        """
        Returns the connection from the engine
        @return: Connection
        """
        return self.__engine.connect()

    def set_engine(self, new_engine):
        """
        Dynamically change the engine used.
        Highly discouraged!
        @param new_engine:
        @return: engine
        """
        self.__engine = new_engine
        return self.__engine

    def set_session(self, new_session):
        """
        Dynamically change the session used.
        @param new_session:
        @return: session
        """
        self.__session = new_session
        return self.__session

    def get_configuration(self):
        """
        Returns the configuration used to connect
        @return: database.Configuration.Configuration.Connection
        """
        return self.__connection_config

    def get_session(self):
        """
        @return: Session|scoped_session
        """
        if self.__session:
            return self.__session
        raise DatabaseException(message="Session was not set properly")


class SessionManager(Manager):
    """
    Manages the session given
    """
    def __init__(self, session):
        """
        Expects a session object that is capable of transactions.
        :param session:
        """
        Manager.__init__(self)
        self.session = session

    def get_session(self):
        """
        Returns the session
        :return:
        """
        return self.session

    def commit(self):
        """
        Commits the session and rolls back the session in case of an error
        :return:
        """
        try:
            self.session.commit()
        except (OperationalError, Exception) as e:
            self.rollback()
            raise DatabaseException(message=e.message, previous=e)

    def rollback(self):
        """
        Rolls back the session
        :return:
        """
        self.session.rollback()

    def add(self, entity):
        """
        Adds the entity to the session/transaction
        :param entity:
        :return:
        """
        try:
            self.session.add(entity)
            return entity
        except (OperationalError, Exception) as e:
            raise DatabaseException(message=e.message, previous=e)

    def delete(self, entity):
        """
        Deletes the entity, better said, adds the delete statement to the session
        :param entity:
        :return:
        """
        try:
            self.session.delete(entity)
            return entity
        except Exception as e:
            raise DatabaseException(message=e.message, previous=e)
