Repositories - Database
=======================

Relevance of Repositories
-------------------------
+ Repositories help organize the queries per entity, that's why the
design for this repositories tend to have the ```<EntityName>Repository```
+ If you wish to write your own Repository for your own Entities, you can
use the ```RepositoryFactory```, with this factory pattern you can load 
and create the desired repository by just passing a string to the factory.
    - The Factory is found in ```database.repositories.factory``` package.
    - If you want to use this factory, the repository needs to implement
    the repository interface found in ```database.repositories.repositories```
    named ```BaseRepository```.
    - In the same way, the EntityManager passed has to implement the interface
    defined in ```database.configuration.entity_manager``` called ```DefaultEntityManager```.    
+ Demo code for using dynamic instantiated repositories.

```python
from Database.repositories.factory.RepositoryFactory
from datetime import datetime

repo = RepositoryFactory.create(
    "some.package.bla.ClassName",
    entity_manager,  # entity manager inherited from DefaultEntityManager
    dict()  # some arguments extra for the repository
    )

rate = repo.get_rate_with_check_in(check_in=datetime.now())  # example query call

```

Contact
-------
+ Arnulfo Solis
    - arnulfo.solis@trivago.com