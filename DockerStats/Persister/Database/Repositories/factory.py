from ..exceptions import RepositoryCreationError
from ..Utility.Validators.repositories import *


class RepositoryFactory(object):
    """
    Factory for the allocation of repositories
    """
    @staticmethod
    def create(full_repository_class, manager, **kwargs):
        """
        Creates the repository with the passed arguments, kwarg arguments are used to instantiate the repository
        :param full_repository_class:
        :param manager:
        :param kwargs: Additional parameters given to the loaded repository
        :return:
        """
        from ....Utility.loaders import ClassLoader

        try:
            if not ManagerInstanceValidator.validate(manager):
                raise RepositoryCreationError("The manager given must inherit from the Manager interface")
            new_repo = ClassLoader.load(full_class_name=full_repository_class)
            if not RepositoryClassValidator.validate(new_repo):
                raise RepositoryCreationError("The repository must be a subclass of the Repository instance")
            repository = new_repo(manager=manager, **kwargs)
            return repository
        except (RepositoryCreationError, TypeError):
            return None
