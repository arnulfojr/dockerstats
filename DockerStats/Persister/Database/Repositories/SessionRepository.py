from .repositories import BaseRepository
from ..Entities.BaseEntities import LoggingSession


class SessionRepository(BaseRepository):
    """
    Session repository
    """
    def __init__(self, manager):
        """
        Manager
        :param manager:
        """
        BaseRepository.__init__(self, manager=manager)

    def create_session(self, container_id, expunge=True):
        """
        Creates a session
        :param container_id:
        :param expunge:
        :return:
        """
        session = LoggingSession()
        session.container_id = container_id
        self.manager.get_session().add(session)
        self.commit()
        if expunge:
            self.manager.get_session().expunge(session)
        return session

    def get_session_with_id(self, session_id):
        """
        Returns the session
        :param session_id:
        :return:
        """
        session = self.manager.get_session().query(LoggingSession).filter_by(
            id=session_id
        ).first()
        return session
