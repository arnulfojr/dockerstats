from .repositories import BaseRepository


class StateRepository(BaseRepository):
    """
    State Repository
    """
    def __init__(self, manager):
        """
        Init with an entity manager
        :param manager:
        """
        BaseRepository.__init__(self, manager=manager)

    def insert(self, entity, log_session, auto_commit=True, create_metadata=False):
        """
        Inserts the data in the database
        :param entity:
        :param log_session:
        :param auto_commit:
        :param create_metadata:
        :return:
        """
        entity.session = log_session
        if create_metadata:
            self.manager.set_up()
        self.add(entity=entity)
        if auto_commit:
            self.commit()
        return entity
