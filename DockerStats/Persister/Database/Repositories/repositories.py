from ..exceptions import DatabaseException


class Repository(object):
    """
    Repository interface
    """
    def __init__(self, manager):
        self.manager = manager

    def commit(self):
        raise NotImplementedError

    def add(self, entity):
        raise NotImplementedError

    def delete(self, entity):
        raise NotImplementedError

    def rollback(self):
        raise NotImplementedError

    def add_collection(self, entities):
        raise NotImplementedError


class BaseRepository(Repository):
    """
    Base repository which sets the minimum structure for a repository
    """
    def __init__(self, manager):
        """
        @param manager: Manager
        """
        Repository.__init__(self, manager=manager)

    def commit(self):
        """
        Attempt to commit the session, if fails rolls back the transaction and raises an exception
        @return:
        """
        try:
            self.manager.get_session().commit()
        except Exception as e:
            self.rollback()
            raise DatabaseException(message=e.message, previous=e)

    def rollback(self):
        self.manager.get_session().rollback()

    def add(self, entity):
        """
        Adds an entity to the Session, this works as long as the Entity is mapped in the
        SQLAlchemy ORM
        @param entity: KSession | KItem | KRates | KayakTrivago Entities
        @return: KSession | KItem | KRates | KayakTrivago Entities
        """
        self.manager.get_session().add(entity)
        return entity

    def add_collection(self, entities):
        """
        Adds the collection of entities passed
        @param entities: list
        @raise TypeError: if passed something else than a list
        @return: list
        """
        if not isinstance(entities, list):
            raise TypeError(message="Adding collections method expects a list")
        self.manager.get_session().add_all(entities)
        return entities

    def delete(self, entity):
        """
        Adds the deletion of the entity to the Session
        @param entity: KSession | KItem | KRates | KayakTrivago Entities
        """
        self.manager.get_session().delete(entity)

    def merge(self, entity):
        """
        Merges the entity to the session
        :param entity:
        :return:
        """
        self.manager.get_session().merge(entity)
        return entity
