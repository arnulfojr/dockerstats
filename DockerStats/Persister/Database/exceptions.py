from DockerStats.exceptions import DockerApiBaseException
from sqlalchemy.exc import StatementError


class BaseDatabaseException(DockerApiBaseException):
    """
    Base Exception of all Hydra Database Exceptions
    """
    def __init__(self, message, code=500, previous=None):
        DockerApiBaseException.__init__(
            self,
            message=message,
            code=code,
            previous=previous
        )

    def get_sql_code(self):
        """
        returns the SQL Error code
        :return:
        """
        if isinstance(self.previous, StatementError):
            return self.previous.orig[0]
        return self.code

    def get_sql_message(self):
        """
        Returns the message from the previous SQL exception
        :return:
        """
        if isinstance(self.previous, StatementError):
            return self.previous.orig[1]
        return self.message


class DatabaseException(BaseDatabaseException):
    """
    Exception raised in problems related to the Database
    """
    def __init__(self, message, code=502, previous=None):
        BaseDatabaseException.__init__(self, message=message, code=code, previous=previous)


class LoaderException(BaseDatabaseException):
    """
    Loader exception
    """
    def __init__(self, message, code=505, previous=None):
        BaseDatabaseException.__init__(self, message=message, code=code, previous=previous)


class LoadingError(LoaderException):
    """
    Exception class that holds the error while loading a Class
    """
    def __init__(self, message, code=506, previous=None):
        LoaderException.__init__(self, message=message, code=code, previous=previous)


class BuilderException(BaseDatabaseException):
    """
    Builder exception
    """
    def __init__(self, message, code=501, previous=None):
        BaseDatabaseException.__init__(self, message=message, code=code, previous=previous)


class BuilderFactoryException(BaseDatabaseException):
    """
    Builder factory exception
    """
    def __init__(self, message, code=502, previous=None):
        BaseDatabaseException.__init__(self, message=message, code=code, previous=previous)


class RepositoryException(BaseDatabaseException):
    """
    Exception coming from the repositories
    """
    def __init__(self, message, code=503, previous=None):
        BaseDatabaseException.__init__(self, message=message, code=code, previous=previous)


class MultipleInstances(RepositoryException):
    """
    Raised when more than one instance was found when it shouldn't
    """
    def __init__(self, message, code=507, previous=None):
        RepositoryException.__init__(self, message=message, code=code, previous=previous)


class RepositoryCreationError(RepositoryException):
    """
    Exception if an error happened in the creation of the repository
    """
    def __init__(self, message, code=504, previous=None):
        BaseDatabaseException.__init__(self, message=message, code=code, previous=previous)
