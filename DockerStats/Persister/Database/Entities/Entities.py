from sqlalchemy import Column, Float
from .BaseEntities import Base, BaseResourceMixin


class ResourceState(BaseResourceMixin, Base):
    """
    Representation of a Session
    """
    __tablename__ = 'simple_state'
    total_cpu_usage = Column(type_=Float, name="total_cpu_usage")
    mem_usage = Column(type_=Float, name="mem_usage")
    mem_limit = Column(type_=Float, name="mem_limit")
    mem_max_usage = Column(type_=Float, name="mem_max_usage")


class ResourceCPUState(BaseResourceMixin, Base):
    """
    Representation of a simple State + CPU percentage
    """
    __tablename__ = 'cpu_state'
    total_cpu_usage = Column(type_=Float)
    cpu_percentage = Column(type_=Float)
    mem_usage = Column(type_=Float)
    mem_limit = Column(type_=Float)
    mem_max_usage = Column(type_=Float)
