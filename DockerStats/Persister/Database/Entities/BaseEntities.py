from sqlalchemy import Column, ForeignKey, Float, Integer, TIMESTAMP, String
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from datetime import datetime


Base = declarative_base()

default_collation = "utf8_unicode_ci"


class BaseEntity(object):
    """
    Base Entity
    """
    created_on = Column(
        name="logged_on",
        type_=TIMESTAMP,
        nullable=False,
        default=datetime.now()
    )


class LoggingSession(BaseEntity, Base):
    """
    Logging session
    """
    __tablename__ = "sessions"
    id = Column(type_=Integer, primary_key=True)
    container_id = Column(type_=String(64), nullable=False)


class BaseResourceMixin(BaseEntity):
    """
    Base resource
    """
    id = Column(type_=Integer, primary_key=True)

    @declared_attr
    def session_id(cls):
        return Column('session_id', ForeignKey('sessions.id'))

    @declared_attr
    def session(cls):
        return relationship("LoggingSession")
