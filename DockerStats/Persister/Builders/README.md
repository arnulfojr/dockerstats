Builders - Database
===================

Builders
--------
+ As stated before in the main documentation, the definition of
the ```BUILDERS``` in Hydra's settings map the type of Builder with the
type of Entity.
    - This means that the configuration is expected to be a dictionary,
    defined as a ```Entity```: ```Builder-Class``` mapping.
    - So in example, an Entity wants to change its Builder Class, the
    new Builder is found in the package ```demo.builders.default``` and
    the class name is ```MainBuilder``` for the entity ```User```, then
    the settings will state as:
        + Code: ```BUILDERS = { "User": "demo.builders.default.MainBuilder" }```.
        + This will automatically use the new Builder to populate and
        persist the instance given.
+ Difference between the ```build``` and the ```populate``` method,
    - They lay in principle that the ```build``` method will allocate if
    not given an instance of the entity that is meant to build, where
    the ```populate``` method simply populates the entity itself, in other
    words will map the given arguments to the entity.
     + Short handed: Use the ```build``` method to get a fresh entity,
     use ```populate``` method to fill the given entity.
     
     
Contact
-------
+ Arnulfo Solis
    - arnulfo.solis@trivago.com