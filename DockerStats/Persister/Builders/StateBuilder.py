from .builder import Builder
from DockerStats.Persister.Database.Entities.Entities import ResourceState, ResourceCPUState
from datetime import datetime


class StateBuilder(Builder):
    def __init__(self):
        Builder.__init__(self)

    def build(self, data, **kwargs):
        """
        Builds the instance
        :param data:
        :param kwargs:
        :return:
        """
        resource_state = ResourceState()
        self.populate(resource_state, data=data)
        return resource_state

    def populate(self, instance, **kwargs):
        """
        Populates the instance with an expected set of data
        :param instance:
        :param data:
        :param kwargs:
        :return:
        """
        data = kwargs.get("data")
        instance.mem_limit = data["memory"]["limit"]
        instance.mem_usage = data["memory"]["usage"]
        instance.mem_max_usage = data["memory"]["max_usage"]
        instance.total_cpu_usage = data["cpu"]["total_usage"]
        instance.created_on = datetime.now()
        return instance


class StateCPUBuilder(StateBuilder):
    def __init__(self):
        StateBuilder.__init__(self)

    def build(self, data, **kwargs):
        """
        Builds the instance based in the State Builder
        :param data:
        :param kwargs:
        :return:
        """
        resource_state = ResourceCPUState()
        self.populate(resource_state, data=data)
        return resource_state

    def populate(self, instance, **kwargs):
        """
        Populates the instance with an expected set of data
        :param instance:
        :param kwargs:
        :return:
        """
        data = kwargs.get("data")
        instance = super(StateCPUBuilder, self).populate(instance, **kwargs)
        instance.cpu_percentage = data["cpu"]["percentage"]
        return instance
