class Builder(object):
    """
    Interface for Builders to implement.
    """

    default_date_parser = "%Y-%m-%d"

    def populate(self, instance, *args, **kwargs):
        """
        Meant to populate the KSession passed with the given data
        @param instance: Entity Instance
        @param kwargs: dict
        @return: Entity Instance
        """
        raise NotImplementedError

    def build(self, *args, **kwargs):
        """
        @param kwargs:
        @raise NotImplementedError:
        """
        raise NotImplementedError
