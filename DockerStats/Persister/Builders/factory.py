from builder import Builder
from DockerStats.Persister.Database.exceptions import BuilderFactoryException


class BuilderFactory(object):

    @staticmethod
    def create(full_builder_class, **kwargs):
        """
        Creates and validates the given Builder Class
        @param full_builder_class: string
        @param kwargs: dict, arguments to pass to the instantiation of the Builder
        @return: Instantiated builder class
        @raise BuilderFactoryException: If validation and allocation fails
        """
        from ...Utility.loaders import ClassLoader
        try:
            new_builder = ClassLoader.load(full_class_name=full_builder_class)
            my_builder = new_builder(**kwargs)  # Instantiate the class
        except Exception as e:
            raise BuilderFactoryException(message=e.message, previous=e)

        if not isinstance(my_builder, Builder):
            raise BuilderFactoryException("The given class is not an instance of <database.Builders.builder.Builder>")

        return my_builder
