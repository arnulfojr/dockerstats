class Manager(object):
    """
    Manager interface
    """
    def get_session(self):
        """
        Should return a session
        :return:
        """
        raise NotImplementedError
