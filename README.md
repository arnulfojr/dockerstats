Docker Resource Logger
======================

Introduction
------------
+ DockerStats aims to take samples of a Docker Instance and persist the samples into a database or wished fixed storage
file.
+ DockerStats aims to be as flexible and overhead-free as possible.

Concepts
--------
+ The concept of DockerStats is plain simple.

Run your samples
----------------
+ Run the ```listen.py``` script, add your containers ID's in the ```config.py``` file.

Defining your containers
------------------------
+ DockerStats was design for flexibility, therefore DockerStats allows the definition of ```Extractors```.
    - Extractors allow the 

Environment Variables
---------------------
+ ```PARALLEL```: This can be ```ENABLED``` or ```DISABLED```.
    - This option enabled the parallel sampling of different containers, each sampling will run in a complete
    independent thread, so in case of this container going away, it wont destroy the sampling of other containers.
    - The ```EXTRACTORS``` definition in the Configuration file ```config.py``` states the ones to use.
    - Extractors execute the part of extracting the information from the Docker API.
        - Extractors consist in three parts ```extractor```, ```processor``` and ```caller```.   
        - ```caller```: This instance class is the one that calls whatever API to use, default Caller calls the Docker API
        - ```processor```: This instance class consists in processing the data extracted from the Caller and processing it
        to the wished data structure.
        - ```extractor```: This instance class wraps the calls of the Caller and the Processor. On instantiation DockerStats
        will inject the loaded Caller and Processor into the Extractor, this shall return the final processed data.
            + This Class is the one that will be called by DockerStats to get the information.
            + Implementation is up to the developer :)
    - The data structure required for it to be able to define a Extractor may be found in code sample 1.
    - For the next step DockerStats needs to persist the data in someway, for this DockerStats benefits from SQLAlchemy Library. 

```python
# CODE SAMPLE 1

EXTRACTORS = dict()
EXTRACTORS["cpu_percentage"] = {
    "extractor": None, # will use the default Extractor Class
    "processor": "DockerStats.Stats.Processors.SimpleDockerStats.SimpleCPUProcessor",
    "caller": None  # will use the default Caller Class
}
```

Contact
-------
+ Arnulfo Solis
+ arnulfojr94@gmail.com
+ Twitter: @arnulfojr